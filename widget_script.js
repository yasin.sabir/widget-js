var Main_Section = document.createElement('div');
Main_Section.setAttribute("id","appointment_section");  

var head  = document.getElementsByTagName('head')[0];
var link  = document.createElement('link');
link.rel  = 'stylesheet';
link.type = 'text/css';
link.href = 'C:/xampp/htdocs/Projects/Widget%20Editing/style.css';
link.media = 'all';
head.appendChild(link);

var today = new Date();
var dd = today.getDate();
var mm = today.getMonth()+1; 
var yyyy = today.getFullYear();

if(dd<10) 
{
    dd='0'+dd;
} 

if(mm<10) 
{
    mm='0'+mm;
} 
today = yyyy+'-'+mm+'-'+dd;


document.body.onload = function addElement () { 

var fname               = "";
var lname               = "";
var email               = "";
var phone               = "";
var move_in_date        = "";
var move_in_time        = "";
var app_Date            = "";
var app_Time            = "";
var min_price           = "";
var max_price           = "";
var notes               = "";



// var Main_Section = document.getElementById('appointment_section');

var html_form = '<div class="custom-container custom-container-bg p-b-30">'+
                '<form action="" name="widget-Form">'+
                    '<div class="custom-form-row">'+
                    '<div class="col-12">'+
                            '<h2 class="custom-font-color">Contact Information</h2>'+
                        '</div>'+
                    '</div>'+
                    '<div class="custom-form-row">'+
                        '<div class="custom-form-group col-6">'+
                            '<label for="" class="custom-font-label">Appointment Date & Time:</label>'+
                            '<div class="col-6 p-l-0">'+
                                '<input type="date" name="app_Date" id="app_Date" class="custom-form-control" required>'+
                            '</div>'+
                            '<div class="col-6 p-l-0">'+
                                '<input type="time" name="app_Time" id="app_Time" class="custom-form-control" >'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                    '<div class="custom-form-row">'+
                        '<div class="custom-form-group col-6">'+
                                '<label for="" class="custom-font-label">First Name:</label>'+
                                '<input type="text" name="first_name" id="first_name" class="custom-form-control" >'+
                                '<span class="validation"></span>'+
                        '</div>'+
                        '<div class="custom-form-group col-6">'+
                                '<label for="" class="custom-font-label">Last Name:</label>'+
                                '<input type="text" name="last_name" id="last_name" class="custom-form-control" >'+
                                '<span class="validation"></span>'+
                        '</div>'+
                    '</div>'+
                    '<div class="custom-form-row">'+
                        '<div class="custom-form-group col-12">'+
                                '<label for="" class="custom-font-label">Email:</label>'+
                                '<input type="email" name="email" id="email" class="custom-form-control" >'+
                                '<span class="validation"></span>'+
                        '</div>'+
                    '</div>'+
                    '<div class="custom-form-row">'+
                        '<div class="custom-form-group col-12">'+
                                '<label for="" class="custom-font-label">Phone Number:</label>'+
                                '<input type="text" name="phone_number" id="phone_number" class="custom-form-control" >'+
                                '<span class="validation"></span>'+
                        '</div>'+
                    '</div>'+
                    '<div class="custom-form-row">'+
                        '<div class="custom-form-group col-6">'+
                                '<label for="" class="custom-font-label">Move in Date & Time:</label>'+
                                '<div class="col-6 p-l-0">'+
                                    '<input type="date" name="move_Date" id="move_Date" class="custom-form-control" >'+
                                    '<span class="validation"></span>'+
                                '</div>'+
                                '<div class="col-6 p-l-0">'+
                                    '<input type="time" name="move_Time" id="move_Time" class="custom-form-control" >'+
                                    '<span class="validation"></span>'+
                                '</div>'+
                            '</div>'+
                        '<div class="custom-form-row">'+
                        '</div>'+
                    '</div>'+
                    '<div class="custom-form-row">'+
                        '<div class="custom-form-group col-6">'+
                                '<label for="" class="custom-font-label">Min Price:</label>'+
                                '<input type="text" name="min_price" id="min_price" class="custom-form-control">'+
                                '<span class="validation"></span>'+
                        '</div>'+
                        '<div class="custom-form-group col-6">'+
                                '<label for="" class="custom-font-label">Max Price:</label>'+
                                '<input type="text" name="max_price" id="max_price" class="custom-form-control" >'+
                                '<span class="validation"></span>'+
                        '</div>'+
                    '</div>'+
                    '<div class="custom-form-row">'+
                        '<div class="custom-form-group col-12">'+
                                '<label for="" class="custom-font-label">Notes:</label>'+
                                '<textarea type="text" name="notes" id="notes" class="custom-form-control"></textarea>'+
                        '</div>'+
                    '</div>'+
                    '<div class="custom-form-row">'+
                        '<div class="custom-form-group col-12">'+
                                '<input type="submit" value="Submit" id="submit_btn" class="custom-btn custom-btn-primary">'+
                        '</div>'+
                    '</div>'+
                '</form>'+
                '<div id="response">'+
                    '<p id="data-1"></p>'+
                    '<p id="data-2"></p>'+
                '</div>'+
                '</div>';    

Main_Section.innerHTML = html_form;  

var currentDiv = document.createElement('div');
    currentDiv.setAttribute("class" , "container-wrapper");

    //afterDiv = document.getElementsByClassName("container-wrapper")[0];
    afterDiv = document.getElementById("WidgetJS");               
    document.body.insertBefore(Main_Section, afterDiv);

    app_Date = document.getElementById("app_Date");
    app_Date.setAttribute("min",today);

    move_in_date = document.getElementById("move_Date");
    move_in_date.setAttribute("min",today);

    document.getElementById("submit_btn").addEventListener("click", function(event){
        event.preventDefault();

        var v1 = document.forms["widget-Form"]["first_name"].value;
        var v2 = document.forms["widget-Form"]["last_name"].value;
        var v3 = document.forms["widget-Form"]["email"].value;
        var v4 = document.forms["widget-Form"]["phone_number"].value;
        var v5 = document.forms["widget-Form"]["min_price"].value;
        var v6 = document.forms["widget-Form"]["max_price"].value;
        var v7 = document.forms["widget-Form"]["notes"].value;
   
        var number_format = /[0-9]/;

        if (v1 == "") {
            document.getElementsByClassName('validation')[0].innerHTML = "First Name is Required";
            document.getElementsByClassName('validation')[0].style.color = "Red";
            document.forms["widget-Form"]["first_name"].focus();
        
        }else{
            document.getElementsByClassName('validation')[0].innerHTML = "";
        }

        if(v2 == ""){
            document.getElementsByClassName('validation')[1].innerHTML = "Last Name is Required";
            document.getElementsByClassName('validation')[1].style.color = "Red";
            document.forms["widget-Form"]["last_name"].focus();
        }else{
            document.getElementsByClassName('validation')[1].innerHTML = "";
        }

        if (v3 == "") {
            document.getElementsByClassName('validation')[2].innerHTML = "Email is Required";
            document.getElementsByClassName('validation')[2].style.color = "Red";
            document.forms["widget-Form"]["email"].focus() ;
        }else{
            validateEmail();
        }

        if (v4 == "") {
            document.getElementsByClassName('validation')[3].innerHTML = "Phone Number is Required";
            document.getElementsByClassName('validation')[3].style.color = "Red";
            document.forms["widget-Form"]["phone_number"].focus();
        }else{
            if((document.forms["widget-Form"]["phone_number"].value.match(number_format))){
                document.getElementsByClassName('validation')[3].innerHTML = "";
            }else{
                document.getElementsByClassName('validation')[3].innerHTML = "";
                document.getElementsByClassName('validation')[3].innerHTML = "Phone Number must be in number format";
                document.getElementsByClassName('validation')[3].style.color = "Red";
                document.forms["widget-Form"]["phone_number"].focus();
            }                
        }


        if (v5 == "") {
            document.getElementsByClassName('validation')[6].innerHTML = "Min Price is Required";
            document.getElementsByClassName('validation')[6].style.color = "Red";
            document.forms["widget-Form"]["min_price"].focus();
       
        }else{
            if((document.forms["widget-Form"]["min_price"].value.match(number_format))){
                document.getElementsByClassName('validation')[6].innerHTML = "";
            }else{
                document.getElementsByClassName('validation')[6].innerHTML = "";
                document.getElementsByClassName('validation')[6].innerHTML = "Price must be in number format";
                document.getElementsByClassName('validation')[6].style.color = "Red";
                document.forms["widget-Form"]["min_price"].focus();
            }
        }

        if (v6 == "") {
            document.getElementsByClassName('validation')[7].innerHTML = "Max Price is Required";
            document.getElementsByClassName('validation')[7].style.color = "Red";
            document.forms["widget-Form"]["max_price"].focus();
        }else{  
           if((document.forms["widget-Form"]["max_price"].value.match(number_format))){
                document.getElementsByClassName('validation')[7].innerHTML = "";
            }else{
                document.getElementsByClassName('validation')[7].innerHTML = "";
                document.getElementsByClassName('validation')[7].innerHTML = "Price must be in number format";
                document.getElementsByClassName('validation')[7].style.color = "Red";
                document.forms["widget-Form"]["max_price"].focus();
            }
        }

        var minP = parseFloat(document.forms["widget-Form"]["min_price"].value);
        var maxP = parseFloat(document.forms["widget-Form"]["max_price"].value);

        if( minP <= maxP ){
            document.getElementsByClassName('validation')[6].innerHTML = "";
            document.getElementsByClassName('validation')[7].innerHTML = "";    
        }else{
            document.getElementsByClassName('validation')[6].innerHTML = "";
            document.getElementsByClassName('validation')[6].innerHTML = "Min Price must be less than or equal Max Price";
            document.getElementsByClassName('validation')[6].style.color = "Red";
            document.forms["widget-Form"]["min_price"].focus();
        }



        function validateEmail() {
            var emailID = document.forms["widget-Form"]["email"].value;
            atpos = emailID.indexOf("@");
            dotpos = emailID.lastIndexOf(".");
            
            if (atpos < 1 || ( dotpos - atpos < 2 )) {
                //document.getElementsByClassName('validation')[2].innerHTML = "";
                document.getElementsByClassName('validation')[2].innerHTML = "Enter email in correct format";
                document.getElementsByClassName('validation')[2].style.color = "Red";
                document.forms["widget-Form"]["email"].focus();
                return false;
            }
            document.getElementsByClassName('validation')[2].innerHTML = "";
        }


        fname        = v1;
        lname        = v2;
        email        = v3;
        phone        = v4;

        app_Date = document.getElementById("app_Date").value;
        app_Time = document.getElementById("app_Time").value;
        var appDatetime = app_Date+" "+app_Time;    

        move_in_date = document.getElementById("move_Date").value;
        move_in_time = document.getElementById("move_Time").value;
        var moveInDatetime = move_in_date+" "+move_in_time;    

        min_price    = v5;
        max_price    = v6;
        notes        = v7;

        //alert(appDatetime+"-"+fname+"-"+lname+"-"+email+"-"+phone+"-"+moveInDatetime+"-"+min_price+"-"+max_price+"-"+notes);

        var toSend = {
                "appointment":{
                    // "start":"2020-01-29T11:15:00",
                    "start" :appDatetime,
                    "location":"",
                    "broker_booked":false
                },
                "client":{
                    "broker_first_name":"tester",
                    "broker_last_name":"testing",
                    "broker_company":"Testing Company",
                    "broker_phone":"0987654321",
                    "broker_email":"testing@getonlineimpact.com",
                    "people":[
                        {
                            "first_name":fname,
                            "last_name":lname,
                            "email":email,
                            "phone_1":phone
                        }
                    ],
                    "community": 105,
                    "sms_opted_in":10,
                    // "move_in_date":"2019-07-01T00:00:00",
                    "move_in_date":moveInDatetime,
                    "layout":null,
                    "price_floor":min_price,
                    "price_ceiling":max_price,
                    "pets":[
                        "20",
                        "10"
                    ],
                    "discovery_source":270,
                    "notes":notes ,
                    "neighborhoods":[
                        133,
                        116,
                        1732,
                        108,
                        375,
                        126
                    ],
                    "elevator":true,
                    "laundry":[
                        20,
                        10
                    ],
                    "outdoor_space":[
                        10
                    ]
                }
        };

        var body = JSON.stringify(toSend); 
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {

                if (this.readyState == 4 && this.status == 200) {
                    var obj = JSON.parse(this.responseText);

                    document.getElementById("data-1").style.color = "Green";
                    document.getElementById("data-2").style.color = "Green";

                    // document.getElementById("data-1").innerHTML =
                    //         obj.data.appointment.confirmation_enabled;

                    document.getElementById("data-2").innerHTML = "Scheduled Confirmed - "+obj.data.appointment.start;
                    
                    // Success State
                    console.log(obj.data.appointment.confirmation_enabled);
                    console.log(obj.data.appointment.start);
                }

                if (this.readyState == 4 && this.status == 400) {
                    var obj1 = JSON.parse(this.responseText);
                
                    document.getElementById("data-1").style.color = "red";
                    document.getElementById("data-1").innerHTML =
                            obj1.errors.appointment.start
                    //Error
                    console.log(obj1.errors.appointment.start);     
                }
        };

        xhttp.open("POST", "https://nestiolistings.com/api/v2/appointments/group/753/book/?key=96bf9b4e0e7b49ae96ac3f4920632747", true);
        //xhttp.open("POST", "getdata.php", true);
        xhttp.setRequestHeader("Content-type", "application/json"); 
        xhttp.send(body);

    });        

}      